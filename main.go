package main

import (
	"net/http"
	"bchain"
	"flag"
)

var port = flag.Int("p", 8000, "port for listen")
var asTracker = flag.Bool("t", false, "run as tracker")
var trackerPort = flag.Int("T", 8000, "tracker port")

func init() {
	flag.Parse()
}

func handle(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		return
	}
	w.Write([]byte("tested"))
}

func main() {
	if *asTracker {
		var t bchain.Tracker
		t.Run(*port)
	} else {
		var c bchain.Client
		c.Run(*port, *trackerPort)
	}




}
