package bchain

import (
	"testing"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"bytes"
)

func TestGetNodes(t *testing.T) {
	r, err := http.Get(`http://localhost:8000/get_nodes`)
	if err != nil {
		t.Error(err)
	}

	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		t.Error(err)
	}

	var b []byte
	err = json.Unmarshal(buf, &b)
	if err != nil {
		t.Errorf("error: %s, buf: %s", err, string(buf))
	}
}

func TestRegNode(t *testing.T)  {
	body := []byte(`8001`)
	r := bytes.NewReader(body)

	_, err := http.Post(`http://localhost:8000/reg_node`,
		"", r)
	if err != nil {
		t.Error(err)
	}
}