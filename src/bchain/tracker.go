package bchain

import (
	"net/http"
	"encoding/json"
	"io/ioutil"
	"strconv"
	"fmt"
)

type Tracker struct {
	nodes []int
	port  int
}

func (t *Tracker) getNodes(w http.ResponseWriter, r *http.Request) {
	b, err := json.Marshal(t.nodes)
	if err != nil {
		panic(err)
	}

	w.Write(b)
}

func (t *Tracker) regNode(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		w.WriteHeader(404)
		return
	}

	res, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}

	port, err := strconv.Atoi(string(res))
	if err != nil {
		panic(err)
	}

	for _, v := range t.nodes {
		if v == port {
			w.WriteHeader(404)
			return
		}
	}

	t.nodes = append(t.nodes, port)
	w.WriteHeader(202)
}

func (t *Tracker) Run(port int)  {
	t.port = port

	for !checkPort(t.port) {
		t.port++
	}

	http.HandleFunc("/get_nodes", t.getNodes)
	http.HandleFunc("/reg_node", t.regNode)
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(404)
	})

	addr := fmt.Sprintf("localhost:%d", t.port)

	if err := http.ListenAndServe(addr, nil); err != nil {
		panic(err)
	}
}