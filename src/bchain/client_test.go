package bchain

import (
	"testing"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"bytes"
)

func TestGetBlocks(t *testing.T) {
	r, err := http.Get(`http://localhost:8000/get_blocks`)
	if err != nil {
		t.Error(err)
	}

	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		t.Error(err)
	}

	var b Blocks
	err = json.Unmarshal(buf, &b)
	if err != nil {
		t.Errorf("error: %s, buf: %s", err, string(buf))
	}
}

func TestSendBlock(t *testing.T) {
	body := []byte(`{"Address":
"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDThGLB9ZYzYI2aX20JO7vjLd9wqvdtuTMHjBvbif/EOemto6pHz0hkApD2qxHMJ5C87uC0hjZlvdsKgPaEpMAQN6WZGXqxNBPi2ouYnUiCYa2HM9he1S5tJyh1CNO3rtaL3zTG3sptUgECADVNw46aXJqtEBWaXv1ofr41FuRCTQIDAQAB","Time":"2018-03-07T17:47:38.751726665+03:00",
"Hash":"cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e",
"Transactions":null}
`)
	r := bytes.NewReader(body)

	_, err := http.Post(`http://localhost:8000/accept_blocks`,
		"", r)
	if err != nil {
		t.Error(err)
	}
}

func TestSendTransaction(t *testing.T) {
	body := []byte(`{"Address": "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDThGLB9ZYzYI2aX20JO7vjLd9wqvdtuTMHjBvbif/EOemto6pHz0hkApD2qxHMJ5C87uC0hjZlvdsKgPaEpMAQN6WZGXqxNBPi2ouYnUiCYa2HM9he1S5tJyh1CNO3rtaL3zTG3sptUgECADVNw46aXJqtEBWaXv1ofr41FuRCTQIDAQAB", 
"Transaction": {"Time":"2018-03-07T17:47:56.380572061+03:00","Sign":"b1a098952e9a480adede2efd3526eeea172183084c69ab89168d9194fea33523a6597e2c2d5728d95f66ae42d6c1b5ba2d47f9aa50d98fba0f8e658fda335994c9b9b6b978bd6dd1c6597adfba9e80da0f745d94b9f8d5274a7a4834d0fd71fa576f92ff7f635c6554f643a719ba442468736c68b1197032f778da6f34781ed9","Info":"foobar"}}`)
	r := bytes.NewReader(body)

	_, err := http.Post(`http://localhost:8000/accept_trans`,
		"", r)
	if err != nil {
		t.Error(err)
	}
}