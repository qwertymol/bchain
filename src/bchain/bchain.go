package bchain

import (
	"time"
	"io/ioutil"
	"encoding/json"
	"os"
	"fmt"
	"crypto/rsa"
	"encoding/base64"
	"strings"
	"crypto/x509"
	"errors"
	"crypto/sha512"
	"crypto"
	"crypto/rand"
)

var keysFile = "keys"

type Transaction struct {
	Time time.Time
	Sign string
	Info string
}

type Block struct {
	Address      string
	Time         time.Time
	Hash         string
	Transactions []Transaction
}

type Blocks []Block

func ReadFromFile(fileName string) Blocks {
	buf, err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil
	}

	var res Blocks
	err = json.Unmarshal(buf, &res)
	if err != nil {
		return nil
	}

	return res
}

func (b *Blocks) getJsonOfBlocks() ([]byte, error) {
	if len(*b) == 0 {
		return []byte("[]"), nil
	}

	return json.Marshal(*b)
}

func (b *Blocks) WriteToFile(fileName string) error {
	f, err := os.Create(fileName)
	if err != nil {
		return err
	}

	buf, err := b.getJsonOfBlocks()
	if err != nil {
		return err
	}

	f.Write(buf)

	return nil
}

func WriteKeyPair(fileName, private, public string) {
	f, _ := os.OpenFile(fileName, os.O_CREATE | os.O_APPEND | os.O_WRONLY, 0644)
	defer f.Close()

	f.WriteString(fmt.Sprintf("%s %s\n", public, private))
}

func DecodeKeys(private string, public string) (*rsa.PrivateKey, error) {
	var pri rsa.PrivateKey
	var pub rsa.PublicKey

	buf1, err := base64.StdEncoding.DecodeString(strings.TrimSpace(private))
	if err != nil {
		return nil, err
	}

	buf2, err := base64.StdEncoding.DecodeString(strings.TrimSpace(public))
	if err != nil {
		return nil, err
	}

	priptr, err := x509.ParsePKCS1PrivateKey(buf1)
	if err != nil {
		return nil, err
	}
	pri = *priptr

	buf, err := x509.ParsePKIXPublicKey(buf2)
	if err != nil {
		return nil, err
	}

	pub = *buf.(*rsa.PublicKey)

	if pri.PublicKey.E == pub.E {
		return &pri, nil
	}

	return nil, errors.New("wrong pair")
}

func (b *Block) GetTransactionsHash() []byte {
	if len(b.Transactions) == 0 {
		return nil
	}

	buf, _ := json.Marshal(b.Transactions)
	h := sha512.New()
	h.Write(buf)

	out := h.Sum(nil)

	return out
}

func (b *Blocks) GetBlockIdBy(address string) int {
	for i, v := range *b {
		if v.Address == address {
			return i
		}
	}

	return -1
}

func (b *Blocks) CreateTransaction(login, pass, text string) (*Transaction, error) {
	key, err := DecodeKeys(pass, login)
	if err != nil {
		return nil, fmt.Errorf("Incorrect values! error: %s", err)
	}

	blockI := b.GetBlockIdBy(login)

	var block Block
	if blockI == -1 {
		return nil, errors.New("No block for these values!")
	}

	block = (*b)[blockI]

	h := sha512.New()
	h.Write([]byte(text))
	hash := h.Sum(nil)

	sign, err := rsa.SignPKCS1v15(rand.Reader, key, crypto.SHA512, hash)

	if err != nil {
		return nil, fmt.Errorf("Error sign: %s", err)
	}

	tr := Transaction{
		Time: time.Now(),
		Info: text,
		Sign: fmt.Sprintf("%x", sign),
	}

	(*b)[blockI].Transactions = append(block.Transactions, tr)

	var buf []byte

	if blockI != 0 {
		buf = []byte((*b)[blockI - 1].Address)
	}

	buf = append(buf, block.GetTransactionsHash()...)

	h = sha512.New()
	h.Write(buf)
	hash = h.Sum(nil)

	(*b)[blockI].Hash = fmt.Sprintf("%x", hash)

	return &tr, nil
}

func GenKeyPair() (private string, public string, err error) {
	key, err := rsa.GenerateKey(rand.Reader, 1024)
	if err != nil {
		return "", "", err
	}

	privateKey := x509.MarshalPKCS1PrivateKey(key)
	publicKey, err := x509.MarshalPKIXPublicKey(&key.PublicKey)
	if err != nil {
		return "", "", err
	}

	private = base64.StdEncoding.EncodeToString(privateKey)
	public = base64.StdEncoding.EncodeToString(publicKey)
	err = nil

	return
}

func (b *Blocks) CreateBlock() (*Block, error) {
	pri, pub, err := GenKeyPair()
	if err != nil {
		return nil, err
	}

	WriteKeyPair(keysFile, pri, pub)

	var buf []byte

	if len(*b) != 0 {
		buf = []byte((*b)[len(*b) - 1].Address)
	}

	h := sha512.New()
	h.Write(buf)
	hash := h.Sum(nil)

	block := Block{
		Address: pub,
		Time: time.Now(),
		Hash: fmt.Sprintf("%x", hash),
	}

	*b = append(*b, block)

	fmt.Printf("Login (address): %s\nPassword: %s\n", pub, pri)

	return &block, nil
}

func (b *Blocks) Print() {
	for i, v := range *b {
		fmt.Printf("%d:\n", i)
		fmt.Printf("\tAddress: %s\n", v.Address)
		fmt.Printf("\tTime: %s\n", v.Time)
		fmt.Printf("\tHash: %s\n", v.Hash)
		fmt.Printf("\tTransactions: \n")

		for i, t := range v.Transactions {
			fmt.Printf("\t%d:\n", i)
			fmt.Printf("\t\tTime: %s\n", t.Time)
			fmt.Printf("\t\tSign: %s\n", t.Sign)
			fmt.Printf("\t\tInfo: %s\n", t.Info)
		}
	}
}