package bchain

import (
	"net/http"
	"fmt"
	"io/ioutil"
	"encoding/json"
	"crypto/sha512"
	"strconv"
	"net"
	"crypto/rsa"
	"encoding/base64"
	"strings"
	"crypto/x509"
	"crypto"
	"encoding/hex"
	"bytes"
)

var (
	blocksFile string
)

type Client struct {
	b    		Blocks
	port 		int
	nodes		[]int
	trackerPort int
}

func checkPort(port int) bool {
	host := ":" + strconv.Itoa(port)
	server, err := net.Listen("tcp", host)

	if err != nil {
		return false
	}

	server.Close()

	return true
}

// get blocks...
func (c *Client) getBlocks(w http.ResponseWriter, r *http.Request) {
	b, err := c.b.getJsonOfBlocks()
	if err != nil {
		panic(err)
	}

	w.Write(b)
}

// accept block...
func (c *Client) acceptBlock(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		w.WriteHeader(404)
		return
	}

	res, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}

	var b Block
	err = json.Unmarshal(res, &b)
	if err != nil {
		panic(err)
	}

	var buf []byte
	if len(c.b) != 0 {
		buf = []byte(c.b[len(c.b) - 1].Address)
	}
	buf = append(buf, b.GetTransactionsHash()...)

	h := sha512.New()
	h.Write(buf)
	hash := h.Sum(nil)
	hashStr := fmt.Sprintf("%x", hash)

	if hashStr == b.Hash {
		c.b = append(c.b, b)
	}

	w.WriteHeader(202)
}

type record struct {
	Address     string
	Transaction Transaction
}

//accept transaction...
func (c *Client) acceptTransaction(w http.ResponseWriter, r *http.Request) {
	res, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}

	if len(res) == 0 {
		w.WriteHeader(404)
		return
	}

	var rec record
	err = json.Unmarshal(res, &rec)
	if err != nil {
		panic(err)
	}

	buf1, err := base64.StdEncoding.DecodeString(strings.TrimSpace(rec.Address))
	if err != nil {
		panic(err)
	}

	//fmt.Println("address", rec.Address)
	pubptr, err := x509.ParsePKIXPublicKey(buf1)
	if err != nil {
		panic(err)
	}
	pub := pubptr.(*rsa.PublicKey)

	hash := sha512.Sum512([]byte(rec.Transaction.Info))

	b, _ := hex.DecodeString(rec.Transaction.Sign)

	err = rsa.VerifyPKCS1v15(pub, crypto.SHA512, hash[:], b);
	//fmt.Println(err)

	if err == nil {
		blockId := c.b.GetBlockIdBy(rec.Address)
		if blockId == -1 {
			w.WriteHeader(404)
			return
		}

		c.b[blockId].Transactions = append(c.b[blockId].Transactions, rec.Transaction)
	}

	w.WriteHeader(202)
}

func (c *Client) listenRoutine() {
	http.HandleFunc("/get_blocks", c.getBlocks)
	http.HandleFunc("/accept_blocks", c.acceptBlock)
	http.HandleFunc("/accept_trans", c.acceptTransaction)
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(404)
	})

	addr := fmt.Sprintf("localhost:%d", c.port)

	if err := http.ListenAndServe(addr, nil); err != nil {
		panic(err)
	}
}

func (c *Client) sendBlockToAll(b *Block) {
	c.getNodes()

	for _, v :=range c.nodes {
		buf, err := json.Marshal(*b)
		if err != nil {
			panic(err)
		}

		r := bytes.NewReader(buf)
		_, err = http.Post(
			fmt.Sprintf("http://localhost:%d/accept_blocks", v),
			"",
			r,
			)
		if err != nil {
			panic(err)
		}
	}
}

func (c *Client) sendTransactionToAll(addr string, t *Transaction) {
	c.getNodes()
	//fmt.Println(*t)

	for _, v :=range c.nodes {
		b, err := json.Marshal(record{addr, *t})
		if err != nil {
			panic(err)
		}

		r := bytes.NewReader(b)
		_, err = http.Post(
			fmt.Sprintf("http://localhost:%d/accept_trans", v),
			"",
			r,
		)
		if err != nil {
			//fmt.Println(v, b)
			panic(err)
		}
	}
}

func (c *Client) getBlocksFrom(port int) Blocks {
	r, err := http.Get(fmt.Sprintf("http://localhost:%d/get_blocks", port))
	if err != nil {
		return nil
	}

	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}

	var b Blocks
	err = json.Unmarshal(buf, &b)
	if err != nil {
		panic(err)
	}
	return b
}

func (c *Client) register() {
	body := []byte(string(strconv.Itoa(c.port)))
	r := bytes.NewReader(body)

	_, err := http.Post(
		fmt.Sprintf("http://localhost:%d/reg_node", c.trackerPort),
		"", r)
	if err != nil {
		panic(err)
	}

	c.getNodes()
	var b Blocks
	for _, v := range c.nodes {
		blocks := c.getBlocksFrom(v)
		if len(b) < len(blocks) {
			b = blocks
		}
	}
	c.b = b
}

func (c *Client) getNodes() {
	r, err := http.Get(fmt.Sprintf("http://localhost:%d/get_nodes", c.trackerPort))
	if err != nil {
		panic(err)
	}

	buf, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(buf, &c.nodes)
	if err != nil {
		panic(err)
	}

	var nodes []int
	for _, v := range c.nodes {
		if v != c.port {
			nodes = append(nodes, v)
		}
	}

	c.nodes = nodes
}

func (c *Client) menu() {
	for {
		var choice int

		fmt.Printf("Choose:\n\t1.Create user\n\t2.Login\n\t3.Print blocks\n\t0.Exit\n")
		fmt.Scan(&choice)

		switch choice {
		case 1: {
			b, _ := c.b.CreateBlock()
			c.sendBlockToAll(b)
		}
		case 2: {
			var login, pass, text string

			fmt.Println("Please auth:")
			fmt.Println("Login:")
			fmt.Scan(&login)
			fmt.Println("Password:")
			fmt.Scan(&pass)
			fmt.Println("Enter text:")
			fmt.Scan(&text)

			t, err := c.b.CreateTransaction(login, pass, text)
			if err == nil {
				c.sendTransactionToAll(login, t)
			}
		}
		case 3:
			c.b.Print()
		default:
			return

		}

		c.b.WriteToFile(blocksFile)
	}
}

func (c *Client) Run(port, trackerPort int) {
	c.port = port

	for !checkPort(c.port) {
		c.port++
	}

	blocksFile = fmt.Sprintf("blocks:%d", c.port)
	c.b = ReadFromFile(blocksFile)

	c.trackerPort = trackerPort
	c.register()

	go c.listenRoutine()

	c.menu()
}